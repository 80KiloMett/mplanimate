import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mplanimate",
    version="0.0.1",
    author="Paul Hillmann - 80KiloMett",
    author_email="paul.hillmann@posteo.de",
    description="A package to facilitate the use of matplotlib's animation \
                 module",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/80KiloMett/mplanimate",
    packages=setuptools.find_packages(
        exclude=["test", "notebooks", ".trash", "video"]
    ),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires=['numpy', 'matplotlib']
)
