import numpy as np
import matplotlib.pyplot as plt
import mplanimate.utils as utils
from matplotlib.animation import ArtistAnimation


class FromFunc():

    def __init__(self, f_of_t, fkwargs=None, xarray=None, tl_params=None):

        self.fkwargs = {}
        if fkwargs is not None:
            self.set_fkwargs(**fkwargs)
        self.f_of_t = np.vectorize(f_of_t, excluded=list(self.fkwargs.keys()))

        self.xarray = np.array([])
        if xarray is not None:
            self.xarray = xarray

        self.tl_params = {
            't0': 0,
            'duration': 5,  # duration in units of t
            'tspeed': 1,  # "duration in t units"/"real time duration"
            'fps': 30,  # frame rate
            'rt_duration': None  # duration of animation in real time
            # if rt_duration is set it overrides tspeed
        }
        if tl_params is not None:
            self.set_tl_params(**tl_params)

        plt.ioff()
        self.fig, self.ax = plt.subplots(1, 1)

    def set_tl_params(self, **tl_params):
        self.tl_params.update(tl_params)

    def set_fkwargs(self, **fkwargs):
        self.fkwargs.update(fkwargs)

    def _mk_artists(self):
        xx, tt = np.meshgrid(self.xarray, self.timeline)
        values = self.f_of_t(tt, xx, **self.fkwargs)
        return [self.ax.plot(self.xarray, v, color='purple') for v in values]

    def __call__(self, xarray=None, tl_params=None):
        if xarray is not None:
            self.xarray = xarray
        if tl_params is not None:
            self.set_tl_params(**tl_params)

        self.timeline = utils.mk_timeline(**self.tl_params)
        artists = self._mk_artists()

        frametime = 1000/self.tl_params['fps']

        self.anim = ArtistAnimation(
            self.fig, artists, interval=frametime, blit=True
        )
        return self.anim


class FromData():

    def __init__(self, fdata=None, xarray=None, fps=30):

        self.xarray = np.array([])
        if xarray is not None:
            self.xarray = xarray

        self.fdata = np.array([])
        if self.fdata is not None:
            self.fdata = fdata

        self.fps = fps

        plt.ioff()
        self.fig, self.ax = plt.subplots(1, 1)

    def _mk_artists(self):
        fdata, xarray = self.fdata, self.xarray
        return [self.ax.plot(xarray, f, color='purple') for f in fdata]

    def __call__(self, fdata=None, xarray=None, fps=None):
        if xarray is not None:
            self.xarray = xarray
        if fdata is not None:
            self.fdata = fdata
        if fps is not None:
            self.fps = fps

        artists = self._mk_artists()

        frametime = 1000/self.fps

        self.anim = ArtistAnimation(
            self.fig, artists, interval=frametime, blit=True
        )

        return self.anim
