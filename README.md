# Installation

```
pip install git+https://gitlab.com/80KiloMett/mplanimate.git
```

# Usage

```python
import mplanimate
```

Use the class 'FromData' to animate already computed data.
Here's an example:
```python
import numpy as np

def sinsin(t, x, omega, k):
    return np.sin(omega * t) * np.sin(k * x)

x = np.linspace(0, 2*np.pi, 50)
t = np.linspace(0, 10, 300)
xx, tt = np.meshgrid(x, t)

f = sinsin(tt, xx, 4, 2)

# mplanimate.FromData(fdata, xarray, fps)
anim = mplanimate.FromData(f, x, 30)

anim().save("animation.mp4")
```
fdata is a 2D numpy array. Each row will be a frame in the animation. The above
example should result in an animation that is 10s long with 30 fps.

The 'FromFunc' class lets you animate the output of a function. You can
predefine fps, a duration of the animation (in the reference frame of the given
function) and an animation speed ("duration in the function reference frame"/"duration in the real time reference frame").
More doc later.
