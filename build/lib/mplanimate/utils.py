import numpy as np


def mk_timeline(t0, duration, tspeed, fps, rt_duration=None):
    if rt_duration is not None:
        tspeed = duration / rt_duration
    maxt = t0 + duration
    dt = tspeed / fps
    return np.arange(t0, maxt, dt)
