import os
import numpy as np
from mplanimate import mplanimate


try:
    os.mkdir("video")
except FileExistsError:
    pass


def sinsin(t, x, omega, k):
    return np.sin(omega * t) * np.sin(k * x)


x = np.linspace(0, 2*np.pi, 50)
t = np.linspace(0, 10, 300)

anim = mplanimate.FromFunc(sinsin, {'omega': 2, 'k': 2}, x)
anim().save("video/FromFunc.mp4")

xx, tt = np.meshgrid(x, t)
fdata = sinsin(tt, xx, 4, 2)

anim = mplanimate.FromData(fdata, x)
anim().save("video/FromData.mp4")
